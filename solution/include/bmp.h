//
// Created by DNS on 25.10.2022.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION1_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION1_BMP_H
#include "image.h"
#include <stdint.h>
#include <stdio.h>
enum read_status{
    READ_OK = 0,
    NULL_FILE,
    INVALID_SIGNATURE,
    INVALID_BITS_NOT_ENOUGH,
    INVALID_BITS_PAD,
    INVALID_HEADER,
    READ_ERROR
};

enum read_status from_bmp(FILE* infile, struct image* image);

enum write_status{
    WRITE_OK = 0,
    WRITE_ERROR
};

enum o_status{
    OPEN_ERROR,
    OPEN_NORMALLY = 0
};

enum argc_status{
    BAD_ARGC,
    GOOD_ARGC = 0
};

enum write_status to_bmp(FILE* outfile, struct image const *image);
#endif //ASSIGNMENT_IMAGE_ROTATION1_BMP_H
