//
// Created by DNS on 03.01.2023.
//
#include "bmp.h"
#include <stdio.h>

#ifndef ASSIGNMENT_IMAGE_ROTATION1_CHECK_FILE_H
#define ASSIGNMENT_IMAGE_ROTATION1_CHECK_FILE_H
int check(FILE* file);

const char *mess(enum read_status read_status);
#endif //ASSIGNMENT_IMAGE_ROTATION1_CHECK_FILE_H
