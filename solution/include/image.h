//
// Created by DNS on 25.10.2022.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION1_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION1_IMAGE_H
#include <stdint.h>
struct pixel{
    uint8_t b,g,r;
};
struct image{
    uint64_t width, height;
    struct pixel *data;
};
void image_deinitialization_function(struct image);
struct image create(uint64_t width, uint64_t height);
#endif //ASSIGNMENT_IMAGE_ROTATION1_IMAGE_H
