//
// Created by DNS on 04.01.2023.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION1_PIXEL_COORDINATES_H
#define ASSIGNMENT_IMAGE_ROTATION1_PIXEL_COORDINATES_H
#include "image.h"
#include <stdint.h>
uint64_t pixel_coordinates(struct image image, uint64_t x, uint64_t y);
#endif //ASSIGNMENT_IMAGE_ROTATION1_PIXEL_COORDINATES_H
