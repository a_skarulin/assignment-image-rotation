//
// Created by DNS on 25.10.2022.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION1_ROTATE_IMG_H
#define ASSIGNMENT_IMAGE_ROTATION1_ROTATE_IMG_H
#include "image.h"
struct image rotate_img(struct image image);
#endif //ASSIGNMENT_IMAGE_ROTATION1_ROTATE_IMG_H
