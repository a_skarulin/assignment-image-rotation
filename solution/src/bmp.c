//
// Created by DNS on 25.10.2022.
//

#include "bmp.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#pragma pack(push,1)
struct header_bmp{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)
static uint16_t padding(uint64_t width){
    return (uint16_t)width%4;
}
static struct header_bmp creator_bmp(const struct image* image){
    return (struct  header_bmp){
            .bfType = 0x4d42,
            .bfileSize = sizeof(struct header_bmp) + sizeof(struct pixel)*image->height*image->width + padding(image->width)*image->height,
            .bfReserved = 0,
            .bOffBits = sizeof(struct header_bmp),
            .biSize = 40,
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = sizeof(struct pixel)*image->height*image->width + padding(image->width)*image->height,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
}


enum read_status from_bmp(FILE* infile, struct image* image){
    if(infile != NULL) {

        struct header_bmp header_bmp = {0};
        if (fread(&header_bmp, sizeof(header_bmp), 1, infile) < 1) {
            return INVALID_HEADER;
        }
        else if(header_bmp.bfType != 0x4d42 || header_bmp.biSize != 40 || header_bmp.biBitCount!=24 || header_bmp.bfReserved != 0){
            return INVALID_SIGNATURE;
        } else {
            *image = create(header_bmp.biWidth, header_bmp.biHeight);
            for(size_t i =0; i < header_bmp.biHeight; i++){
                if(fread(&image->data[image->width*i], sizeof(struct pixel), header_bmp.biWidth, infile) < header_bmp.biWidth){
                    return INVALID_BITS_NOT_ENOUGH;
                }
                if(fseek(infile, padding(image->width),SEEK_CUR) > 0){
                    return INVALID_BITS_PAD;
                }

            }

            return READ_OK;
        }
    }
    return NULL_FILE;
}


enum write_status to_bmp(FILE* outfile, struct image const *image){
    struct header_bmp header_bmp = creator_bmp(image);

    if(fwrite(&header_bmp, sizeof(struct header_bmp),1, outfile) == 1) {

        for(size_t i = 0; i<image->height;i++) {

            if (fwrite(&(image->data[image->width * i]), sizeof(struct pixel), image->width, outfile) < image->width) {
                return WRITE_ERROR;
            }
            if (image->width % 4 > 0) {
                if (fwrite(&image->data[image->width * i], sizeof(char), padding(image->width), outfile) < padding(image->width)) {
                    return WRITE_ERROR;
                }
            }
        }
    }
    else{
        return WRITE_ERROR;
    }
    return WRITE_OK;
}
