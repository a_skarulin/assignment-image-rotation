//
// Created by DNS on 03.01.2023.
//
#include "bmp.h"
#include "check_file.h"
#include <stdio.h>

int check(FILE* file){
    if(file == NULL || ferror(file)!=0){
        return 1;
    }
    return 0;
}

const char *mess(enum read_status read){
   const char *messag[] = {[INVALID_BITS_NOT_ENOUGH] = "wrong with infile (bits)",[INVALID_SIGNATURE] = "wrong with infile (signatur)",
            [INVALID_HEADER]="wrong with infile (header)", [INVALID_BITS_PAD] = "wrong with padding",
            [NULL_FILE] = "No file"};

    return messag[read];
}
