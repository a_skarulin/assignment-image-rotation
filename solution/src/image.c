//
// Created by DNS on 25.10.2022.
//


#include "image.h"
#include <stdint.h>
#include <stdlib.h>
void image_deinitialization_function(struct image image){
    free(image.data);
}
struct image create(uint64_t width, uint64_t height){
    return (struct image){
        .width=width,
        .height=height,
        .data =  malloc(width*sizeof( struct pixel)*height)
    };

}
