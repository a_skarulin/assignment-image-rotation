#include "bmp.h"
#include "check_file.h"
#include "image.h"
#include "rotate_img.h"
#include <stdio.h>
#include <stdlib.h>
int main( int argc, char** argv ) {
    if(argc != 3){

        fprintf(stderr,"error in number of arguments");
        fprintf(stdout, "failed to rotate image. Sorry please");
        return BAD_ARGC;
    }

    FILE *infile = fopen(argv[1], "rb");
    if(check(infile) == 1){
        fprintf( stderr,"problem with in file");
        fprintf(stdout, "failed to rotate image. Sorry please");
        fclose(infile);
        return OPEN_ERROR;
    }

    FILE *outfile = fopen(argv[2], "wb");
    if(check(outfile) == 1){
        fclose(outfile);
        fprintf(stderr,"problem with out file");
        fprintf(stdout, "failed to rotate image. Sorry please");
        return OPEN_ERROR;
    }

    struct image img = {0};

    enum read_status inread = from_bmp(infile, &img);
    fclose(infile);


    if(mess(inread)){

        fprintf(stderr,"%s", mess(inread));
        fprintf(stdout, "failed to rotate image. Sorry please");
        image_deinitialization_function(img);
        return READ_ERROR;
    }
//    if(inread == INVALID_BITS_NOT_ENOUGHT){
//        printf("wrong with infile (bits)");
//        fprintf(stderr, "failed to rotate image. Sorry please");
//        image_deinitialization_function(img);
//        return ;
//    }
//    if(inread == INVALID_SIGNATURE){
//        fprintf(stderr,"wrong with infile (signatur)");
//        fprintf(stdout, "failed to rotate image. Sorry please");
//        image_deinitialization_function(img);
//        return 1;
//    }
//    if(inread == INVALID_HEADER){
//        fprintf(stderr,"wrong with infile (header)");
//        fprintf(stdout, "failed to rotate image. Sorry please");
//        image_deinitialization_function(img);
//        return 1;
//    }
//    if(inread == INVALID_BITS_PAD){
//        fprintf(stderr,"wrong with padding");
//        fprintf(stdout, "failed to rotate image. Sorry please");
//        image_deinitialization_function(img);
//        return 1;
//    }
//    if(inread == NULL_FILE){
//        fprintf(stderr,"No file");
//        fprintf(stdout, "failed to rotate image. Sorry please");
//        image_deinitialization_function(img);
//        return NULL_FILE;
//    }

    struct image rotated = rotate_img(img);

    enum write_status outwrite = to_bmp(outfile, &rotated);
    fclose(outfile);
    if(outwrite == WRITE_ERROR){
        fprintf(stderr,"Some wrong with outfile");
        fprintf(stdout, "failed to rotate image. Sorry please");
        return WRITE_ERROR;
    }


    image_deinitialization_function(img);
    image_deinitialization_function(rotated);

    fprintf(stdout, "EVERYTHING IS FINE!!!");
    return 0;
}
