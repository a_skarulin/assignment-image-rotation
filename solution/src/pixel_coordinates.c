//
// Created by DNS on 04.01.2023.
//
#include "pixel_coordinates.h"
#include "image.h"
#include <stddef.h>
#include <stdint.h>

uint64_t pixel_coordinates(struct image image, uint64_t x, uint64_t y){
        return x*image.height+y;
}
