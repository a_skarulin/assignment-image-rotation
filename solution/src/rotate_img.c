//
// Created by DNS on 25.10.2022.
//
#include "image.h"
#include "pixel_coordinates.h"
#include "rotate_img.h"
#include <stdint.h>
struct image rotate_img(struct image const image_for_rotate){
        struct image result = create(image_for_rotate.height, image_for_rotate.width);
        for (uint64_t i = 0; i < image_for_rotate.width; i++) {
            for (uint64_t j = 0; j < image_for_rotate.height; j++) {
                result.data[pixel_coordinates(image_for_rotate,i,j)] = image_for_rotate.data[i + image_for_rotate.height*image_for_rotate.width - image_for_rotate.width*j - image_for_rotate.width];
            }
        }
        return result;

}
